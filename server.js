var express=require('express');
var morgan=require('morgan');
var mongoose=require('mongoose');
var bodyParser=require('body-parser');
var ejs=require('ejs');
var ejs_mate=require('ejs-mate');

//var session = require('express-session');
//var cookieParser = require('cookie-parser');
//var flash = require('express-flash');
//var MongoStore = require('connect-mongo/es5')(session);
//var passport = require('passport');
//var secret = require('./config/secret');

var User=require('./models/user');
var app=express();
mongoose.connect('',function(err){
    if(err){ console.log(err);}
    else{console.log("connected to the database");}
});

// middleware
app.use(express.static(__dirname + '/public'));
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.engine('ejs',ejs_mate);
app.set('View engine','ejs');

var mainRoutes=require('./routes/main');
app.use(mainRoutes);

app.listen(3000,function(err){
    if(err) throw err;
    console.log("server is running on port 3000");
});