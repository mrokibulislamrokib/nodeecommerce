var passport=require('passport');
var LocalStrategy=require('passport-local').Strategy;
var User=require('../models/user');

passport.serializeUser(function(user,done){
    done(null,user._id);
});

passport.deserializeUser(function(id,done){
    User.findById(id,function(err,user){
        done(err,user);
    });
});


passport.use('local-login',new LocalStrategy({

},function(){
    User.findOne({},function(err,user){
        if(err) return done(err);
    });
}));


exports.isAuthenticate=function(req,res,next){
    if(req.isAuthenticate){
        return next();
    }else{
        res.redirect('/login');
    }
}